import { Environment } from '@sharedInterfaces/environment';

export const environment: Environment = {
  production: true,
  apiKey: 'AIzaSyArVexQysXQ7rEqGQSzdkYtC3lTPf0e1kk',
  fbDbUrl:
    'https://angular-blog-7a788-default-rtdb.europe-west1.firebasedatabase.app',
};
