export interface FirebaseAuthResponse {
  displayName: string;
  email: string;
  idToken: string;
  expiresIn: string;
  kind: string;
  localId: string;
  registered: boolean;
}
