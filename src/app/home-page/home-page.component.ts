import { Component, OnInit } from '@angular/core';
import { PostsService } from '@shared/services/posts.service';
import { Post } from '@sharedInterfaces/post';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  public posts$: Observable<Post[]>;
  constructor(private postsService: PostsService) {}

  ngOnInit(): void {
    this.posts$ = this.postsService.getAll();
  }
}
