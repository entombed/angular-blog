import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from '@sharedInterfaces/user';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  public form: FormGroup;
  public minLength = 6;
  public submitted = false;
  public message: string;
  get emailInvalid(): boolean {
    return this.form.get('email').touched && this.form.get('email').invalid;
  }
  get emailRequired(): boolean {
    return this.form.get('email').errors.required;
  }
  get emailNotEmail(): boolean {
    return this.form.get('email').errors.email;
  }
  get passwordInvalid(): boolean {
    return (
      this.form.get('password').touched && this.form.get('password').invalid
    );
  }
  get passwordRequired(): boolean {
    return this.form.get('password').errors.required;
  }
  get passwordMinlength(): boolean {
    return this.form.get('password').errors.minlength;
  }

  constructor(
    public authService: AuthService,
    private router: Router,
    private activateRouter: ActivatedRoute
  ) {
    this.activateRouterHandler();
  }

  ngOnInit(): void {
    this.initForm();
  }

  protected activateRouterHandler(): void {
    this.activateRouter.queryParamMap.subscribe((params: ParamMap) => {
      if (params.has('loginAgain')) {
        this.message = 'Введите данные для регистрации';
      } else if (params.has('expireSession')) {
        this.message = 'Ваша сессия закончилась, зарегистрируйтесь';
      }
    });
  }

  protected initForm(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(this.minLength),
      ]),
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.authService.error$.next();
    this.submitted = true;
    const user: User = {} as User;
    ({ email: user.email, password: user.password } = this.form.value);
    this.authService.login(user).subscribe(
      (data) => {
        this.form.reset();
        this.router.navigate(['/admin', 'dashboard']);
        this.submitted = false;
      },
      () => {
        this.submitted = false;
      }
    );
  }
}
