import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@sharedInterfaces/user';
import { Observable, Subject, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from 'rxjs/operators';
import { FirebaseAuthResponse } from '@sharedInterfaces/firebase-auth-response';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private url = 'https://identitytoolkit.googleapis.com/v1';
  public error$: Subject<string> = new Subject<string>();
  get token(): string {
    const expDate = new Date(localStorage.getItem('fb-token-exp'));
    if (new Date() > expDate) {
      this.logout();
      return null;
    }
    return localStorage.getItem('fb-token');
  }

  constructor(private http: HttpClient) {}

  public login(user: User): Observable<any> {
    user.returnSecureToken = true;
    return this.http
      .post(
        `${this.url}/accounts:signInWithPassword?key=${environment.apiKey}`,
        user
      )
      .pipe(catchError(this.handleError.bind(this)), tap(this.setToken));
  }

  private handleError(error: HttpErrorResponse): Observable<HttpErrorResponse> {
    const { message } = error.error.error;
    switch (message) {
      case 'INVALID_EMAIL':
        this.error$.next('Неверный email');
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('Неверный пароль');
        break;
      case 'EMAIL_NOT_FOUND':
        this.error$.next('Такого email нет');
        break;
    }
    return throwError(error);
  }

  public logout(): void {
    this.setToken(null);
  }

  public isAuthenticated(): boolean {
    return Boolean(this.token);
  }

  private setToken(responce: FirebaseAuthResponse | null): void {
    if (responce) {
      const expDate = new Date(
        new Date().getTime() + Number(responce.expiresIn) * 1000
      );
      localStorage.setItem('fb-token', responce.idToken);
      localStorage.setItem('fb-token-exp', expDate.toString());
    } else {
      localStorage.clear();
    }
  }
}
