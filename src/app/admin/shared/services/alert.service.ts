import { Injectable } from '@angular/core';
import { Alert } from '@sharedInterfaces/alert';
import { Subject } from 'rxjs';

@Injectable()
export class AlertService {
  public alert$ = new Subject<Alert>();

  constructor() {}

  public success(text: string): void {
    this.alert$.next({ type: 'success', text });
  }
  public warning(text: string): void {
    this.alert$.next({ type: 'warning', text });
  }
  public danger(text: string): void {
    this.alert$.next({ type: 'danger', text });
  }
}
