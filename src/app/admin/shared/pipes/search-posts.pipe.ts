import { Pipe, PipeTransform } from '@angular/core';
import { Post } from '@sharedInterfaces/post';

@Pipe({
  name: 'searchPosts',
})
export class SearchPostsPipe implements PipeTransform {
  transform(posts: Post[], search: string = ''): Post[] {
    if (!search.trim()) {
      return posts;
    }
    return posts.filter((post) =>
      post.title.toLowerCase().includes(search.toLocaleLowerCase())
    );
  }
}
