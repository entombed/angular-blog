import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AlertType } from '@sharedInterfaces/alert';
import { Subscription } from 'rxjs';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit, OnDestroy {
  @Input() delay = 5000;

  protected sub = new Subscription();
  public text = '';
  public type: AlertType = 'success';

  constructor(protected alertService: AlertService) {}

  ngOnInit(): void {
    this.initAlertSubscription();
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  initAlertSubscription(): void {
    this.sub.add(
      this.alertService.alert$.subscribe((alert) => {
        ({ type: this.type, text: this.text } = alert);
        const timeout = setTimeout(() => {
          clearTimeout(timeout);
          this.text = '';
        }, this.delay);
      })
    );
  }
}
