import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostsService } from '@shared/services/posts.service';
import { Post } from '@sharedInterfaces/post';
import { Subscription } from 'rxjs';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  private sub: Subscription = new Subscription();
  public posts: Post[] = [];
  public searchString = '';
  constructor(
    private postsService: PostsService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.sub.add(
      this.postsService.getAll().subscribe((posts) => {
        this.posts = posts;
      })
    );
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  remove(id: string): void {
    this.sub.add(
      this.postsService.remove(id).subscribe(() => {
        this.posts = this.posts.filter((post) => post.id !== id);
        this.alertService.success('Пост удален');
      })
    );
  }
}
