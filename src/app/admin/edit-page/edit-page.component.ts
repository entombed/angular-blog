import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { PostsService } from '@shared/services/posts.service';
import { Post } from '@sharedInterfaces/post';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
})
export class EditPageComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public isUpdating: boolean;
  private post: Post;
  private sub = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private postService: PostsService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.initActivateRouteSubscribe();
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  initActivateRouteSubscribe(): void {
    this.route.params
      .pipe(
        switchMap((params: Params) => {
          return this.postService.getById(params.id);
        })
      )
      .subscribe((post: Post) => {
        this.post = post;
        this.form = new FormGroup({
          title: new FormControl(post.title, Validators.required),
          text: new FormControl(post.text, Validators.required),
        });
      });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.isUpdating = true;
    const post: Post = {
      ...this.post,
      text: this.form.value.text,
      title: this.form.value.title,
    };
    this.sub.add(
      this.postService.update(post).subscribe(
        () => {
          this.isUpdating = false;
          this.alertService.success('Пост обновлен');
        },
        () => {
          this.isUpdating = false;
        }
      )
    );
  }
}
