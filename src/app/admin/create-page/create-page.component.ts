import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostsService } from '@shared/services/posts.service';
import { Post } from '@sharedInterfaces/post';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss'],
})
export class CreatePageComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private postsService: PostsService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  protected initForm(): void {
    this.form = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      text: new FormControl(null, [Validators.required]),
      author: new FormControl(null, [Validators.required]),
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    const post: Post = {} as Post;
    post.date = new Date();
    ({
      title: post.title,
      text: post.text,
      author: post.author,
    } = this.form.value);
    this.postsService.create(post).subscribe(() => {
      this.form.reset();
      this.alertService.success('Пост успешно добавлен');
    });
  }
}
